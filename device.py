# This file is part of sale_pos module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields

class SaleDevice(metaclass=PoolMeta):
    __name__ = 'sale.device'

    change_unit_price = fields.Boolean('Change of Price')
    allow_credit = fields.Boolean('Allow Credit')

    def default_allow_credit():
        return False
