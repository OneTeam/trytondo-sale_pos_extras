# This file is part of sale_pos module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool, PoolMeta
from trytond.exceptions import UserError
from trytond.model import ModelView, fields
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport
from trytond.pyson import Eval



class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    allow_credit_device = fields.Function(fields.Boolean('Allow Credit'), 'get_device_allow_credit')

    
    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls._buttons.update({
                'full_to_done': {
                    'invisible': ~Eval('allow_credit_device', True) | ~Eval('state').in_(['draft']),
                    'readonly': ~Eval('lines', []),
                    'depends': ['allow_credit_device', 'state'] 
                    },
        })

    @fields.depends('sale_device')
    def get_device_allow_credit(self, name):
        return self.sale_device.allow_credit


    @classmethod
    @ModelView.button
    def full_to_done(cls,sales):
        '''Lleva las ventas desde el estado borrador(draft) hasta el estado finalizado(done).
           En estre proceso crea la factura y los movimientos de stock de forma automática,
           además de aprobarlos y contabilizarlos.'''
        
        Invoice = Pool().get('account.invoice')
        Shipment = Pool().get('stock.shipment.out')
        ShipmentReturn = Pool().get('stock.shipment.out.return')
        
        if sales:
            for sale in sales:
                if len(sale.lines)<1:
                    cls.raise_user_error('La venta{s} no está en estado borrador o no contiene lineas'.foramt(s=sale))
                    return
        else:
            cls.raise_user_error('Error en las venatas seleccionadas')
            return
        
        cls.quote(sales)
        cls.confirm(sales)
        cls.process(sales)
        Transaction().commit()#se graba la informacion para poder acceder a las facturas y los shipments
        sales = cls.browse([x.id for x in sales])#se vuelve a leer para cargar informacion de la base de datos, verificar si es necesario.
        sales = [x for x in sales if (x.state == 'processing' and len(x.lines) >0 ) ]#solo las ventas en borrador
        if len(sales) <= 0:
            cls.raise_user_error('Falló la finalización automática de la(s) venta(s)')
            return
        for sale in sales:
            invoices = sale.invoices
            if invoices:
                for invoice in invoices:
                    if not invoice.invoice_date:
                        invoice.invoice_date = sale.sale_date
                        Invoice.save([invoice])
                Invoice.validate_invoice(invoices)
                Invoice.post(invoices)
            shipments = sale.shipments
            if shipments:
                for shipment in shipments:
                    shipment.effective_date = sale.sale_date
                    Shipment.save([shipment])
                Shipment.assign_wizard(shipments)
                Shipment.pack(shipments)
                Shipment.done(shipments)
            shipment_returns = sale.shipment_returns
            if shipment_returns:
                for shipment_return in shipment_returns:
                    shipment_return.effective_date = sale.sale_date
                    ShipmentReturn.save([shipment_return])
                ShipmentReturn.receive(shipment_returns)

class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    change_unit_price_device = fields.Function(fields.Boolean('Change Unit Price'), 'get_device_change_unit_price')

    
    @classmethod
    def __setup__(cls):
        super(SaleLine, cls).__setup__()
        cls.unit_price.depends += ['change_unit_price_device']
        cls.unit_price.states.update({
            'readonly': ~Eval('change_unit_price_device', True),
            })
        # Allow edit product, quantity and unit in lines without parent sale
        for fname in ('product', 'quantity', 'unit'):
            field = getattr(cls, fname)
            if field.states.get('readonly'):
                del field.states['readonly']


    @fields.depends('sale')
    def get_device_change_unit_price(self, name):
        return self.sale.sale_device.change_unit_price


        
class WizardSalePayment(metaclass=PoolMeta):
    __name__ = 'sale.payment'
    print_ = StateReport('sale_pos.sale_ticket')

    def default_start(self, fields):
        Sale = Pool().get('sale.sale')
        sale = Sale(Transaction().context['active_id'])
        result = super(WizardSalePayment, self).default_start(fields)
        result['self_pick_up'] = sale.self_pick_up
        return result

    def transition_pay_(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        active_id = Transaction().context.get('active_id', False)
        sale = Sale(active_id)
        result = super(WizardSalePayment, self).transition_pay_()
        Sale.print_ticket([sale])
        if len(sale.lines)>0:
            Sale.full_to_done([sale])
        if result == 'end':
            return 'print_'
        return result

    def transition_print_(self):
        return 'end'

    def do_print_(self, action):
        data = {}
        data['id'] = Transaction().context['active_ids'].pop()
        data['ids'] = [data['id']]
        return action, data

