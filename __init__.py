from trytond.pool import Pool
from . import sale
from . import device

def register():
    Pool.register(
            sale.Sale,
            sale.SaleLine,
            device.SaleDevice,
            module='sale_pos_extras', type_='model')
    Pool.register(
            sale.WizardSalePayment,
            module='sale_pos_extras', type_='wizard')
